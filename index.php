<?php

//Array donde seran guardados los 5 numeros, sin repetirse
$numeros = [];

$i = 0;
while($i <= 4){
    
    $num = rand(1, 48);

    if(!in_array($num, $numeros)){   // Si el numero random no esta en el array
        array_push($numeros, $num); //  sera agregado.
        $i++;
    }
}

function color($n){ // Recibe el numero de bolilla

    switch($n){ 
        // Le asigna el color dependiendo del numero
        case ($n <= 12):
            $color = 'green'; // Verde desde la 1 hasta la 12
            break;

        case ($n >= 13 && $n <= 24):
            $color = 'orange'; // Naranja 13-24    
            break;

        case ($n >= 25 && $n <= 36):
            $color = 'yellow'; // Amarilla 25-36    
            break;
        
        case ($n >= 37 && $n <= 48):
            $color = 'violet'; // Violeta 37-48  
            break;           
    }
    // Retorna el color correspondiente
    return $color;
}


?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Desafio Generador 5 de oro sorpresa">
    <meta name="keywords" content="HTML, CSS, PHP">
    <meta name="author" content="neodev">   
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Generador 5 de Oro Sorpresa</title>
    <link rel="shortcut icon" type="image/x-icon" href="./assets/img/cinco.png">
    <link rel="stylesheet" href="./assets/css/style.css">
    <style>
        body {
            background-image: url("./assets/img/lucky.jpeg");
        }
    </style>
</head>
<body>
    <h1><b>5 DE ORO SORPRESA</b></h1>
    <h2>Hoy: <?= date('d/m/Y')?></h2>
    <div>
        <span style="border: solid 7px <?=color($numeros[0]);?>;"> <?=$numeros[0]?> </span>
        <span style="border: solid 7px <?=color($numeros[1]);?>;"> <?=$numeros[1]?> </span>
        <span style="border: solid 7px <?=color($numeros[2]);?>;"> <?=$numeros[2]?> </span>
        <span style="border: solid 7px <?=color($numeros[3]);?>;"> <?=$numeros[3]?> </span>
        <span style="border: solid 7px <?=color($numeros[4]);?>;"> <?=$numeros[4]?> </span>
        <!--Invoca funcion color y pasa el numero de bolilla como parametro ( color($numeros[]) ). 
        Y se muestra el numero de la bolilla ( $numeros[] ).-->
    </div>
    <br><br>
    <button onclick="window.location.reload();">¡JUGAR!</button>
    <br><br>
    <footer>
        <q>La aplicación está basada en el juego real del 5 de Oro, devolverá números del 1 al 48 sin que ninguno se repita.
        <br>
        Los colores de las bolillas respetan un rango numérico, verde(del 1 al 12), naranja(del 13 al 24)
        amarillo(del 25 al 36) y violeta(del 37 al 48).
        </q>
        <!--Info: https://comosacarel5deoro.blogspot.com/2016/08/estadisticas-5-de-oro.html-->
    </footer>
</body>
</html>